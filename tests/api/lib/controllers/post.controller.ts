import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async createPost(idValue: number, bodyValue: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body({
                authorId: idValue,
                body: bodyValue,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async addComment(idValue: number, postIdValue: number, bodyValue: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body({
                authorId: idValue,
                postId: postIdValue,
                body: bodyValue,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async addLike(idValue: number, likeValue: boolean, userIdValue: number, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body({
                entityId: idValue,
                isLike: likeValue,
                userId: userIdValue,
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }

}