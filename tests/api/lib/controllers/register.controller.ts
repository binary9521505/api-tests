import { ApiRequest } from "../request";
const baseUrl: string = global.appConfig.baseUrl;

export class RegisterController {
    async createUser(emailValue: string, nameValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body({
                email: emailValue,
                userName: nameValue,
                password: passwordValue,
            })
            .send();
        return response;
    }
}