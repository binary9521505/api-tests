import { expect } from "chai";
import { PostController } from "../lib/controllers/post.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import {RegisterController} from "../lib/controllers/register.controller";
import { checkStatusCode, checkResponseTime, checkjsonSchema } from "../../helpers/functionsForChecking.helper";
import { ALL } from "dns";

const post = new PostController();
const users = new UsersController();
const auth = new AuthController();
const register = new RegisterController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Register controller`, () => {

    it(`create new user`, async () => {
        let response = await register.createUser('dasha_doris@gmail.com', 'Dasha', 'Gattaka');

        console.log("New user:");
        console.log(response.body);

        checkStatusCode(response, 201);
        checkResponseTime(response,2000);
        // checkjsonSchema(response, schemas.schema_register); 


    });
});

describe(`Get all posts`, () => {

    it(`get all existing posts`, async () => {
        let response = await post.getAllPosts()

        // console.log("All existing posts:");
        // console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response,2000);
        // checkjsonSchema(response, schemas.schema_getAllPosts); 
         
    });
});

describe(`Post controller`, () => {
    let likeValue = true;
    let postId;
    let accessToken: string;
    let authId: number;
    before(`Login and get the token`, async () => {
        let response = await auth.login("dasha_doris@gmail.com", "Gattaka");

        accessToken = response.body.token.accessToken.token;
        authId = response.body.id;
        // console.log(accessToken);
    });

    it(`Create new post`, async () => {
        let response = await post.createPost(authId, "My fisrt post", accessToken)

        console.log("My new post:");
        console.log(response.body);
        postId = response.body.id
    
        checkStatusCode(response, 200);
        checkResponseTime(response,2000);
         
    });

    it(`Add a comment`, async () => {
        let response = await post.addComment(authId, postId, "nice post", accessToken)

        console.log("New comment");
        console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response,2000);
    })

    it(`Add a reaction`, async () => {
        let response = await post.addLike(postId, likeValue, authId, accessToken)
        console.log("Reaction 'Like' added");
        
        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
    })
 
});

