import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import {RegisterController} from "../lib/controllers/register.controller";
import { checkStatusCode, checkResponseTime, checkUserName, checkjsonSchema } from "../../helpers/functionsForChecking.helper";
import { ALL } from "dns";

const users = new UsersController();
const auth = new AuthController();
const register = new RegisterController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Register controller`, () => {

    it(`create new user`, async () => {
        let response = await register.createUser('dasha_doris@gmail.com', 'Dasha', 'Gattaka');

        console.log("New user:");
        console.log(response.body);

        checkStatusCode(response, 201);
        checkResponseTime(response,2000);
        // checkjsonSchema(response, schemas.schema_register); 


    });
});

describe(`Get all users`, () => {
 
    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await users.getAllUsers();

        // console.log("All Users:");
        // console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response,3000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        checkjsonSchema(response, schemas.schema_allUsers); 
        
    });
});

describe("Positive check for authorization, token usage, and update user", () => {
    let accessToken: string;
    let userDataBeforeUpdate, userDataToUpdate;
    let userId;

    before(`Login and get the token`, async () => {
        let response = await auth.login("dasha_doris@gmail.com", "Gattaka");

        accessToken = response.body.token.accessToken.token;
        console.log(accessToken);
    });

    it(`Info about the current user by token`, async () => {
        
        let response = await users.getCurrentUser(accessToken);
        
        console.log(response.body)
        
        userDataBeforeUpdate = response.body;

        checkStatusCode(response, 200);
        checkResponseTime(response,2000);
        checkUserName(response, 'Dasha')
        // checkjsonSchema(response, schemas.schema_userByToken);
    });

    it(`Update username using valid data`, async () => {
        function replaceLastThreeWithRandom(str: string): string {
            return str.slice(0, -3) + Math.random().toString(36).substring(2, 5);
        }

        userDataToUpdate = {
            id: userDataBeforeUpdate.id,
            avatar: userDataBeforeUpdate.avatar,
            email: userDataBeforeUpdate.email,
            userName: replaceLastThreeWithRandom(userDataBeforeUpdate.userName),
        };

        let response = await users.updateUser(userDataToUpdate, accessToken);
        checkStatusCode(response, 204);
        checkResponseTime(response,2000);

        
    });

    it(`Info about the current user by token after updating`, async () => {
        let response = await users.getCurrentUser(accessToken);
                
        console.log(response.body)
    
        checkStatusCode(response, 200);
        checkResponseTime(response,2000);
        // checkjsonSchema(response, schemas.schema_userByToken);
    });

    it(`Info about current user by id after updating`, async () => {
        let response = await users.getUserById(userDataBeforeUpdate.id);

        console.log(response.body)
        userId = response.body.id;
        
        checkStatusCode(response, 200);
        checkResponseTime(response,2000);
        expect(response.body).to.be.deep.equal(userDataToUpdate, "User details isn't correct");        

    });

    after('Deleting the user', async () => {
        let response = await users.deleteUser(userId, accessToken)
        console.log('Deleted user', response.body)

        checkStatusCode(response, 204);
        checkResponseTime(response,2000);
    });
});




 




