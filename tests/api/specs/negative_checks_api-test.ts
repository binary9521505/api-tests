import {
    checkResponseTime,
    checkStatusCode, 
} from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';
import {RegisterController} from "../lib/controllers/register.controller";

const auth = new AuthController();
const register = new RegisterController();


describe('Use test data set for login using invalid password', () => {
    let invalidCredentialsDataSet = [
        { email: 'datatest@gmail.com', password: '' },
        { email: 'datatest@gmail.com', password: '      ' },
        { email: 'datatest@gmail.com', password: 'Atest300! ' },
        { email: 'datatest@gmail.com', password: 'test 300!' },
        { email: 'datatest@gmail.com', password: 'admin' },
        { email: 'datatest@gmail.com', password: 'datatest@gmail.com' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 401); 
            checkResponseTime(response, 3000);
        });
    });
});

describe('Use test data set for login using invalid email', () => {
    let invalidCredentialsDataSet = [
        { email: 'datatestgmail.com', password: 'test300!' },
        { email: 'datatest@gmailcom', password: 'test300!' },
        { email: 'datatest @gmail.com', password: 'test300!' },
        { email: 'datatest123@gmail.com', password: 'test300!' },
        { email: 'дatatest@gmail.com', password: 'test300!' },
        { email: '@datatestgmail.com', password: 'test300!' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 404); 
            checkResponseTime(response, 3000);
        });
    });
})

describe('Use test data set to register user with empty userName field', () => {
    let invalidCredentialsDataSet = [
        { email: 'test1@gmail.com', userName: '', password: 'emptyName!' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.userName}' + '${credentials.password}'`, async () => {
            let response = await register.createUser(credentials.email, credentials.userName, credentials.password);

            checkStatusCode(response, 400); 
            checkResponseTime(response, 3000);
        });
    });
})


